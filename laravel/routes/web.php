<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// frontend
Route::group([
    'namespace' => 'Frontend'
], function () {
    Route::get('/', 'HomeController@index')->name('home');

    // check not login for form login
    Route::group(['middleware' => 'memberNotLogin'], function () {
        Route::get('/member-login', 'MemberController@showLogin');
        Route::post('/member-login', 'MemberController@login');

        Route::get('/member-register', 'MemberController@showRegister');
        Route::post('/member-register', 'MemberController@register');
    });

    // check login 
    Route::group(['middleware' => 'member'], function () {
        Route::get('/member-profile', 'MemberController@show');
        Route::get('/member-logout','MemberController@logout');
    });
});


// admin
Auth::routes();
//Login manager Route
Route::group([
    'prefix' => 'admin',
    'namespace' => 'Auth'
], function () {
    Route::get('/', 'LoginController@showLoginForm');
    Route::get('/login', 'LoginController@showLoginForm');
    Route::post('/login', 'LoginController@login');
    Route::get('/logout', 'LoginController@logout');
});

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => ['admin']
], function () {
	Route::get('/dashboard','DashboardController@index')->name('admin.dashboard');
    Route::get('/user/profile/{id}','UserController@edit')->name('admin.user.edit');
    Route::post('/user/profile/{id}','UserController@update')->name('admin.user.update');
});

